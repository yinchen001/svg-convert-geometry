﻿using System.Windows;
using System.Windows.Media;

namespace SvgConvertGeometry
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ConvertBtn_Click(object sender, RoutedEventArgs e)
        {
            GeometryTextBox.Foreground = new SolidColorBrush(Colors.Black);
            string geometryStr = "<Geometry x:Key=\"Geometry\">";
            string svgStr = SvgTextBox.Text;
            if (string.IsNullOrEmpty(svgStr))
            {
                GeometryTextBox.Text = "请输入SVG！";
                GeometryTextBox.Foreground = new SolidColorBrush(Colors.Red);
                return;
            }
            string[] splitStr = svgStr.Split("<path");
            for (int i = 1; i < splitStr.Length; i++)
            {
                geometryStr += "\n" + MidStrEx(splitStr[i], "d=\"", "\"");
            }
            geometryStr += "\n" + "</Geometry>";
            GeometryTextBox.Text = geometryStr;
        }
        public string MidStrEx(string sourse, string startstr, string endstr)
        {
            string result = string.Empty;
            int startindex, endindex;
            startindex = sourse.IndexOf(startstr);
            if (startindex == -1)
                return result;
            string tmpstr = sourse.Substring(startindex + startstr.Length);
            endindex = tmpstr.IndexOf(endstr);
            if (endindex == -1)
                return result;
            result = tmpstr.Remove(endindex);
            return result;
        }
    }
}
